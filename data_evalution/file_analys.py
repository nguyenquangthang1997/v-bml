import matplotlib.pyplot as plt
import pandas as pd

path = [{
    "elas": "data_100/eth_elas3.txt",
    "json": "data_100/eth_json3.txt",
    "sql": "data_100/eth_sql3.txt",
    "xml": "data_100/eth_xml3.txt",
    "xlsx": "data_100/eth_xlsx3.txt"
}, {"elas": "data_100/sawtooth_elas2.txt",
    "json": "data_100/sawtooth_json2.txt",
    "sql": "data_100/sawtooth_sql2.txt",
    "xml": "data_100/sawtooth_xml2.txt",
    "xlsx": "data_100/sawtooth_xlsx2.txt"}]

list_draw = []
for i in range(0, 2):
    writer = pd.ExcelWriter('bml/output' + str(i) + '.xlsx', engine='xlsxwriter')
    draw = {
        "elas": {},
        "json": {},
        "sql": {},
        "xml": {},
        "xlsx": {}
    }
    thang_temp = {
        "elas": {},
        "json": {},
        "sql": {},
        "xml": {},
        "xlsx": {}
    }
    for type in path[i]:
        file = open(path[i][type])
        # result = {}
        temp = {"s": 0.0, "m": 0.0}
        # thang = {"s": [], "g": []}
        po = []

        for line in file:
            line_array = line.split("++")
            if line_array[0] not in draw[type]:
                draw[type][line_array[0]] = temp.copy()
                draw[type][line_array[0]]["m"] = float(line_array[2][:-1])
            # else:
            draw[type][line_array[0]][line_array[1]] += float(line_array[2][:-1])

        s = [draw[type][address_]['s'] for address_ in draw[type]]
        m = [draw[type][address_]['m'] for address_ in draw[type]]
        thang_temp[type]['s'] = round(sum(s) / len(s), 3)
        thang_temp[type]['m'] = round(sum(m) / len(m), 3)
        po = [(address, draw[type][address]['s'], draw[type][address]['m']) for address in draw[type]]
        pd.DataFrame(po).to_excel(writer, sheet_name=type)
    list_draw.append(thang_temp)
    writer.save()
print(list_draw)
