from web3 import Web3

infura_url = "https://ropsten.infura.io/v3/8d62942fd62641a7ab758673105b6df3"
web3 = Web3(Web3.HTTPProvider(infura_url))
temp = []
for i in range(0, 64):
    account = web3.eth.account.create()
    print(account.address + "\n")
    temp.append({"address": account.address, "private_key": account.privateKey.hex()})
print(temp)

# temp = [{'address': '0xE9fB290e106eb7715aDD03e90334525160dC3f65',
#          'private_key': '0xc07d2ffb55761c5f8c2cde2b58f0fa332cfcecf714b55ee1474156e072f3cc3f'},
#         {'address': '0xf85a79d980D891628E9D19f06aE70142dEDfC305',
#          'private_key': '0x9985642f8205084023ee13803faeab8c7be449b8612dc07a38509c55bbd12a9f'},
#         {'address': '0x296B511B09828CcC55b1C3fb402d71312AE550b6',
#          'private_key': '0x66876c60957e0a03c0e5e1d9d48fb9ae0bca44e88f74f71ecaacfe37c1936c05'},
#         {'address': '0x59ed8C0ec1361543A6E5D6c841F136d64d00a7b1',
#          'private_key': '0x21228650089f8bd304b31b5f24660c67c9bc678d76d682ed2a41faee8591b938'},
#         {'address': '0xc44615829F96114ffB1447e927dD5b6c87334757',
#          'private_key': '0xda1a11a2d24399c913bd1dc29e224e56a33effd6ce7161ff1c9f2d4f3f6644de'},
#         {'address': '0xD6fF548292B6a3820CEaB485Eccf28c29b977647',
#          'private_key': '0x6c608926fe09707d4d82f9aeb9e97a559618c9b612d788e0cf7d4504f1f479ab'},
#         {'address': '0x88212284b37a1DC2b8400708B6afd63edf1CaF85',
#          'private_key': '0xdbfe9f2be7bcadb82268f94aed899253ffc4a5c68d39f228ea5dc2354b594f47'},
#         {'address': '0xD5964D4AEd1cC26E48274664850576ed9e6ac9a7',
#          'private_key': '0x4fb8714a8bec1a6078094729848df9a0bfc6615989b2daa4412a7a93b62ffc57'},
#         {'address': '0x6Cc4BC1Bcaba471F539401E9B7ca6E2CC043245A',
#          'private_key': '0xbf39dc250e742c1fd7203f7fd6e48e18d0783a63b1c25d4c49f025a22c72d96f'},
#         {'address': '0x4C2834F61Ac5c88De339aB202FAb4ebAD7338926',
#          'private_key': '0x8f3f4c6ca79f3851e69bcc83f1c3570b70b66f66cf062a540bc70fc8b4bdce31'},
#         {'address': '0xa14056D141EDA673E64643Bbb84cf158bb13Ef52',
#          'private_key': '0x06620524a1092225912ba1dd5a34003d40d271da921bd8a2ed4f2cfb18b5daf9'},
#         {'address': '0x9575D29ee95FDE64ed208008935B1A2F8eA89755',
#          'private_key': '0x957327083b81980e610fcb99cfb9b8c607e3d53732876a5b7b85e3a3a5d65010'},
#         {'address': '0x9f09616586B672933807a7F1e75Dc703b15e2F61',
#          'private_key': '0xf94781860f719b5f39484fbeebf09a55d481e5efee7b8961086db96e22b308ec'},
#         {'address': '0x79400FEF5Ba968513E503f23fd3729B37936ADd2',
#          'private_key': '0x4f2fbe120d935f00c96b52d89868c4e45d5f682c716db5096327098f21a67c8c'},
#         {'address': '0xf2a506920B0F2555B8c09b779eA7C2C99253e820',
#          'private_key': '0xbf65364d3c65e567049436417978840f38f248d67d057f1ecbe2e657cb40210b'},
#         {'address': '0xb8a9A4CC20d0Afd0c5428E72Cc99d24a0FfBbA29',
#          'private_key': '0xb8ca6d1ec820b30845187dc26cc57222213b58b673e488e0ff9fbc625ab3e0ca'}]
#
# address = '0x5AF5e0C07e766ebA1Fe1b9Add659d116DEfd1aab'
# private_key = '0533f36b5677d2206413c20a2ba70cbec8a5c3d82d84a3e6d5a34003913107b9'
# # for object_t in temp:
# #     nonce = web3.eth.getTransactionCount("0x5AF5e0C07e766ebA1Fe1b9Add659d116DEfd1aab")
# #     signed_txn = web3.eth.account.signTransaction(dict(
# #         gasPrice=int(web3.eth.gasPrice * 10),
# #         nonce=nonce,
# #         gas=510800,
# #         to=object_t['address'],
# #         data="",
# #         value=1000000000000000000
# #     ),
# #         private_key
# #     )
# #     txn_hash = web3.eth.sendRawTransaction(signed_txn.rawTransaction)
# #     transaction = web3.eth.waitForTransactionReceipt(transaction_hash=txn_hash, timeout=120)
#
# nonce = web3.eth.getTransactionCount("0x5AF5e0C07e766ebA1Fe1b9Add659d116DEfd1aab")
# signed_txn = web3.eth.account.signTransaction(dict(
#     gasPrice=int(web3.eth.gasPrice * 10),
#     nonce=nonce,
#     gas=510800,
#     to="0x9f09616586B672933807a7F1e75Dc703b15e2F61",
#     data="",
#     value=1500000000000000000
# ),
#     private_key
# )
# txn_hash = web3.eth.sendRawTransaction(signed_txn.rawTransaction)
# transaction = web3.eth.waitForTransactionReceipt(transaction_hash=txn_hash, timeout=120)
temp = [{'address': '0xd6344f11487601d07fBd7363C62AA3d8553F855E',
         'private_key': '0xa17d786a22a161f22bacc6711f719b4c92dc0b823643cd96b87fc61a9b8952b1'},
        {'address': '0xad67c52f3b3f5AbDF028f46adB33a12B393fdcBC',
         'private_key': '0xe9932633c9a4e2280e1d23e367bf65dbc3a23c7f2914905968c0c22de8cdbc1a'},
        {'address': '0x9E7828140199CB3c9FF8F979c2A68F66476fD70c',
         'private_key': '0xbc8d0966f5b9ce9d6dc99d69aef79f062cc575095af42e669806bc4810d849bf'},
        {'address': '0x33c1b4049970B458646dC0b8937C95C4546F87d6',
         'private_key': '0x6233cdaef8503788d176ebf91de8b8a7cd9133f61db298035147bcd75e584a1a'},
        {'address': '0x8aF07c5b984D91Ecdd6E75AD15682b776465cb43',
         'private_key': '0x1bbdcdecc412ac3048ea2e9434bc56a117be3841948aafd6c6c0d3c0fa6e996e'},
        {'address': '0x80DF5b99cFd37f39b033336EF6024d7F38D61E04',
         'private_key': '0x32fc7151d904aa60069fe5726c9b42d3b7832d1fcf1fac9739abf84617696203'},
        {'address': '0xAdAC3E81405eF2C315Eb0802B1FCCA5De2C42d23',
         'private_key': '0xe39692a7ecf55a778a5be0f3ddf2dd869720ef89db2871ae47b6876db59f3de6'},
        {'address': '0xF4Fef9a675737b4D2E8665F552524d713166aFbb',
         'private_key': '0x7b68557a3bd578296d16857dab388fb4d2dec871935b1d0c923320864f0d41e8'},
        {'address': '0xb582dB83fE142054fD62A6024Dd2Fb2faDCF5BfF',
         'private_key': '0xdcc4eda41c9f93a3089caca80b2c2a18d743f126d0087c3a6224481ea359c669'},
        {'address': '0xEC9bf06FB58725944D720582550E29f18D1e7753',
         'private_key': '0xf89c20488706f55f25bb3458ff5bc7f4db13e59efd552dc12036f557aec9e496'},
        {'address': '0x338Cb48fc23D856B88419737880E48AE2ec35326',
         'private_key': '0xdaf690445d8e616268d2dfd9389148bb495fcf11e68c59c7dc94c978d6c7a195'},
        {'address': '0x60afC9B1A57ff76CE86E089828CD182C58240951',
         'private_key': '0x73fff115678a014868a1058e425875c8326470330eacc3baefad1752eabff72a'},
        {'address': '0xc14b0DF6dd4A454D98dF0a56310152cC4ec9c42b',
         'private_key': '0xf7356fa7a38cce403f564d7542775ee4f68e5c85b3a5aca8952b3ab864f45e20'},
        {'address': '0x435A38de3243cad9BfB92A52893B5d44cFC81FF4',
         'private_key': '0x17dc3d64c1640dc7128c63fda64d9be50fb2eb0708a4ddffaacc3702526f3cb7'},
        {'address': '0x62A67a67294858baf43166019eabba3939D885c5',
         'private_key': '0xa3ef51caf1aa7841e679ef04a2d34bfc086db4e0cc2727cd89f7662eea6e77ab'},
        {'address': '0xfF302f85306D349998a4526d8b5838A54a4e4449',
         'private_key': '0x0281fb4e74857dcfd4043ec1ce1bb165bb2d929b15e6507dbdb13c96db1d9c0b'},
        {'address': '0x8e9876bAA6A89a60F7414F437455a2D3b998B4D9',
         'private_key': '0x8b49d8a205de077e73827cd0fe867eaf6525be47ab246ee71c87d1cc61213140'},
        {'address': '0xD554c0823a56bC79169ac2759a6459782d89Ad40',
         'private_key': '0x57a77fa2870217a34887860bf4a5430a240d9329b4eb05ac90d61c89996d3009'},
        {'address': '0x17E74480B6dfEDEC468f8B7462A1d8750dA13045',
         'private_key': '0xcb84e93bb94ddc126761f730e3b70544801b86d8a799db25bac0037897191ee9'},
        {'address': '0x1fdd04Ca35dE1B7fE207d435D6e38C09fa503533',
         'private_key': '0x59943384e2f6287890efbeb17adc7d24ec1372e995b37240ce7bd9d3077d0d70'},
        {'address': '0xac70B395FC38a078e64A6892842dFBDEA691efD1',
         'private_key': '0xbbfd10ab98044f71b8573d0099bdf9ea8dc3c250629a937f0cb4f3abbb2f6aae'},
        {'address': '0x540FBb2395192739435d69094Ce36c86bE8FD832',
         'private_key': '0x3bff1643f24a6cb92bee260ef5eebf742e326570344b2995729f49bb79e4404f'},
        {'address': '0x60EFc29350A2BAD94614148a6c15e57a06f0D1e9',
         'private_key': '0xe5d2c56db603e998fab85eab784b5809b8e653866b3525d86e2d3358e5a021c0'},
        {'address': '0x65031f0685D84980684fd95E028804bdEBe94D8b',
         'private_key': '0x8b26e4d917eb6b815114203fe4f1fc0767f389a7818709f5cded0e6dadf4d73b'},
        {'address': '0x126d6a741ea557f6FF7d1869b23eCb841c81B2D6',
         'private_key': '0x4f885274b48a4701b6708ed6673d308e021d24576b214656a5b99b1d4d72ef0c'},
        {'address': '0xC4BeF34e36A1D708982333656D51d792159E02Ed',
         'private_key': '0xb9f273fce6b4054c5e4d09788596b63338fdf3725d63c3e3e5277a3ebdd8991b'},
        {'address': '0xd5eA6964b924281EeCACAc31299B22Ce6Da047fa',
         'private_key': '0x416f026df8e3febdb6818f055415ca377823341785762e1c6cfb0bb25121a833'},
        {'address': '0x5fF9bC4894C3b524A4f9BF9fc5c6dE01C429a81b',
         'private_key': '0xa207cfa8c5321bb49c6f14c5e0096415583a23f08ccd4c6d25b8552dcf8ca630'},
        {'address': '0x4646e2D0c362f6B0F72A5141681fdd8E8be161ee',
         'private_key': '0xe0e709dd7b2daa65107e4d3f3e6c1b5e8ed09b31757b1cf71ba3dcfe4a6a4b15'},
        {'address': '0x3Da85174c124123f635f243a04F60Da9DC689DB6',
         'private_key': '0x9cb13d23a38cc1aadf4111108018327f169126d1912bffb2ec29936ceb8cb557'},
        {'address': '0x3944a6fb1C32a418d3f170DA366AffFa589720C1',
         'private_key': '0x97a26ac268685e7c0cd53b632e5932ade5a56139cbc63ee352a02aa6736f9c2a'},
        {'address': '0x3aAaBdDF167B94840106D84Be5ef0a36901E3993',
         'private_key': '0x79db5576493f138b8e01bf86b7c5736b3a27092ff9ffc207d908d22bd68dcbe6'}]
