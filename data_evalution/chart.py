import numpy as np
import matplotlib.pyplot as plt

# mng = plt.get_current_fig_manager()
# mng.full_screen_toggle()

# bar chart
# data to plot
n_groups = 5
means_frank = (1.355,
               1.435,
               1.252,
               1.277,
               1.305)
means_guido = (0.366,
               0.341,
               0.373,
               0.361,
               0.354
               )
mapping = (0.125, 0.241, 0.062, 0.073, 0.094)

# create plot
fig, ax = plt.subplots(figsize=(12, 7))
index = np.arange(n_groups)
bar_width = 0.3
opacity = 1
index1 = np.arange(n_groups + 1)

rects1 = plt.bar(index, means_frank, bar_width,
                 alpha=opacity,

                 label="Write")

rects2 = plt.bar(index + bar_width, means_guido, bar_width,
                 alpha=opacity,

                 label='Query')
# 
# rects3 = plt.bar(index + bar_width + bar_width, mapping, bar_width,
#                  alpha=opacity,
# 
#                  label='Mapping')

plt.xlabel('Data Types', fontsize=15)
plt.ylim(0, 37)
plt.ylabel('Time(s)', fontsize=15)
# plt.title('Time to save')
plt.tick_params(labelsize=15)

plt.xticks(index + bar_width - 0.15, ('XML', 'XLSX', 'JSON', 'SQL', "NoSQL"), fontsize=15)
plt.legend(prop={'size': 15})
for index, data in enumerate(means_frank):
    plt.text(x=index - 0.15, y=data + 0.2, s=f"{data}", fontdict=dict(fontsize=15))
for index, data in enumerate(means_guido):
    plt.text(x=index - .15 + bar_width, y=data + 0.2, s=f"{data}", fontdict=dict(fontsize=15))

# for index, data in enumerate(mapping):
#     plt.text(x=index - .1 + bar_width + bar_width, y=data + 0.01, s=f"{data}", fontdict=dict(fontsize=15))
plt.tight_layout()
plt.savefig('bml/sawtooth_type.pdf')
plt.close()

n_groups = 5
means_frank = (31.095,
               31.295,
               30.837,
               31.398,
               30.498)
means_guido = (2.289,
               2.228,
               2.302,
               1.995,
               2.126
               )

# mapping = (0.123, 0.280, 0.062, 0.071, 0.087)

# create plot
fig, ax = plt.subplots(figsize=(12, 7))
index = np.arange(n_groups)
index1 = np.arange(n_groups + 1)
bar_width = 0.3
opacity = 1

rects1 = plt.bar(index, means_frank, bar_width,
                 alpha=opacity,

                 label="Write")

rects2 = plt.bar(index + bar_width, means_guido, bar_width,
                 alpha=opacity,

                 label='Query')
# 
# rects3 = plt.bar(index + bar_width + bar_width, mapping, bar_width,
#                  alpha=opacity,
# 
#                  label='Mapping')

plt.xlabel('Data Types', fontsize=15)
plt.ylabel('Time(s)', fontsize=15)
# plt.title('Time to save')
plt.xticks(index + bar_width - 0.15, ('XML', 'XLSX', 'JSON', 'SQL', "NoSQL"), fontsize=15)
plt.legend(prop={'size': 15})
for index, data in enumerate(means_frank):
    plt.text(x=index - 0.13, y=data + 0.2, s=f"{data}", fontdict=dict(fontsize=15))
for index, data in enumerate(means_guido):
    plt.text(x=index - .13 + bar_width, y=data + .2, s=f"{data}", fontdict=dict(fontsize=15))

# for index, data in enumerate(mapping):
#     plt.text(x=index - .13 + bar_width + bar_width, y=data + .2, s=f"{data}", fontdict=dict(fontsize=15))
plt.ylim(0, 37)
plt.tick_params(labelsize=15)

plt.tight_layout()
# plt.show()

plt.savefig('bml/eth_type.pdf')
plt.close()

# create plot
fig, ax = plt.subplots(figsize=(12, 7))
index = np.arange(n_groups)
index1 = np.arange(n_groups + 1)

bar_width = 0.3
opacity = 1
mapping_eth = (0.123, 0.280, 0.062, 0.071, 0.087)
mapping_sawtooth = (0.125, 0.241, 0.062, 0.073, 0.094)

rects1 = plt.bar(index, mapping_sawtooth, bar_width,
                 alpha=opacity,
                 label='Hyperledger Sawtooth')

rects2 = plt.bar(index + bar_width, mapping_eth, bar_width,
                 alpha=opacity,
                 label='Etherum')

plt.xlabel('Data Types', fontsize=15)
plt.ylabel('Time(s)', fontsize=15)
# plt.title('Time to save')
plt.tick_params(labelsize=15)
plt.xticks(index + bar_width - 0.15, ('XML', 'XLSX', 'JSON', 'SQL', "NoSQL"), fontsize=15)
plt.legend(prop={'size': 15})
for index, data in enumerate(mapping_sawtooth):
    plt.text(x=index - 0.15, y=data + 0.001, s=f"{data}", fontdict=dict(fontsize=15))
for index, data in enumerate(mapping_eth):
    plt.text(x=index - .15 + bar_width, y=data + .001, s=f"{data}", fontdict=dict(fontsize=15))
plt.tick_params(labelsize=15)

# for index, data in enumerate(mapping):
#     plt.text(x=index - .13 + bar_width + bar_width, y=data + .2, s=f"{data}", fontdict=dict(fontsize=15))
plt.tight_layout()
# plt.show()

plt.savefig('bml/mapping_type.pdf')
plt.close()

fig, ax = plt.subplots(figsize=(12,8.5))

year = [1, 2, 4, 8, 16]

pop_pakistan = [0.497,
                0.453,
                0.426,
                0.402,
                0.373

                ]
pop_india = [4.816,
             3.006,
             2.298,
             1.915,
             1.277
             ]

plt.plot(year, pop_india, "-go")
plt.plot(year, pop_pakistan, "-go")
plt.plot(year, pop_pakistan, label='Query')
plt.plot(year, pop_india, label="Write")
plt.xlabel('Number of thread', fontsize=25)
plt.ylabel('Time(s)', fontsize=25)
plt.ylim(0, 30)

# plt.ylim(0, 500

# # plt.title('Pakistan India Population till 2010')
plt.legend(prop={'size': 25})

for index, data in enumerate(pop_pakistan):
    flag = 0
    flag1 = 0
    if index == 0:
        flag1 = 0.9
    if index == len(pop_pakistan) - 1:
        flag = 0.1
    plt.text(x=year[index] + flag-flag1, y=data + 0.35 - flag * 3, s=f"{data}", fontdict=dict(fontsize=22))
# plt.text(x=year[4]-9, y=pop_pakistan[4] + 5, s=f"{pop_pakistan[4]}", fontdict=dict(fontsize=15))
for index, data in enumerate(pop_india):
    flag = 0
    if index == len(pop_india) - 1:
        flag = 0.1
    plt.text(x=year[index] + flag, y=data + 0.5, s=f"{data}", fontdict=dict(fontsize=22))
# plt.text(x=year[4] - 0.1, y=pop_india[4] + 5, s=f"{pop_india[4]}", fontdict=dict(fontsize=15))
plt.tick_params(labelsize=25)
plt.xlim(0, 17.9)
plt.tight_layout()
# plt.show()

plt.savefig('bml/sawtooth_thread.pdf', bbox_inches='tight')
plt.close()
#
year = [1, 2, 4, 8, 16]
fig, ax = plt.subplots(figsize=(12, 8.5))

pop_pakistan = [11.882,
                9.939,
                5.375,
                3.279,
                2.019

                ]
pop_india = [445.172,
             318.059,
             155.157,
             73.197,
             30.837

             ]
plt.plot(year, pop_india, "-go")
plt.plot(year, pop_pakistan, "-go")
plt.plot(year, pop_pakistan, label='Query')
plt.plot(year, pop_india, label="Write")
plt.xlabel('Number of thread', fontsize=25)
plt.ylabel('Time(s)', fontsize=25)
plt.ylim(0, 500)
# # plt.title('Pakistan India Population till 2010')
plt.legend(prop={'size': 25})
plt.tick_params(labelsize=25)

for index, data in enumerate(pop_pakistan):
    flag = 0
    if index == 0:
        flag = 0.9
    plt.text(x=year[index] - 0.05 - flag, y=data + 5+flag*5, s=f"{data}", fontdict=dict(fontsize=22))
# plt.text(x=year[4]-9, y=pop_pakistan[4] + 5, s=f"{pop_pakistan[4]}", fontdict=dict(fontsize=15))
for index, data in enumerate(pop_india):
    flag = 0
    if index == len(pop_india) - 1:
        flag = 0.1
    plt.text(x=year[index] - 0.05 - flag, y=data + 5, s=f"{data}", fontdict=dict(fontsize=22))
# plt.text(x=year[4] - 0.1, y=pop_india[4] + 5, s=f"{pop_india[4]}", fontdict=dict(fontsize=15))
plt.xlim(0,17.9)

plt.tight_layout()
# plt.show()
plt.savefig('bml/eth_thread.pdf', bbox_inches='tight')
plt.close()

year = [1000, 2000, 5000, 10000]
fig, ax = plt.subplots(figsize=(12, 8.5))

pop_pakistan = [0.373,
                0.532,
                1.115,
                2.062

                ]
pop_india = [1.277,
             3.359,
             6.692,
             22.039]
#
# # pop_pakistan = [2.019,
# #                 2.433,
# #                 3.831,
# #                 6.819
# #
# #                 ]
# # pop_india = [30.837,
# #              76.881,
# #              165.565,
# #              380.831
# #              ]
#
plt.plot(year, pop_india, "-go")
plt.plot(year, pop_pakistan, "-go")
plt.plot(year, pop_pakistan, label='Query')
plt.plot(year, pop_india, label="Write")
# plt.xlabel('Number of thread')
plt.xlabel('Number of record data', fontsize=25)
plt.ylabel('Time(s)', fontsize=25)
# # plt.title('Pakistan India Population till 2010')
plt.legend(prop={'size': 25})
plt.ylim(0, 30)

# plt.xlim(0, 11000)

for index, data in enumerate(pop_pakistan):
    flag = 0
    flag1 = 0
    flag3 = 0
    if index == 0:
        flag = -85
        flag1 = 0.05
    if index == len(pop_pakistan) - 1:
        flag3 = 400
    plt.text(x=year[index] - 10 - flag - flag3, y=data + .3 - flag1, s=f"{data}", fontdict=dict(fontsize=22))
# plt.text(x=year[4]-9, y=pop_pakistan[4] + 5, s=f"{pop_pakistan[4]}", fontdict=dict(fontsize=15))
for index, data in enumerate(pop_india):
    flag3 = 0
    flag2 = 0
    flag1 = 0
    if index == 0:
        flag1 = 0.2
    if index == len(pop_india) - 1:
        flag3 = 400
    if index == len(pop_india) - 2:
        flag2 = 490
    plt.text(x=year[index] - 200 - flag3 - flag2, y=data + 0.9 - flag3 / 600 - flag2 / 900 + flag1, s=f"{data}",
             fontdict=dict(fontsize=22))
# plt.text(x=year[4] - 0.1, y=pop_india[4] + 5, s=f"{pop_india[4]}", fontdict=dict(fontsize=15))
plt.tick_params(labelsize=25)

plt.tight_layout()
#
# plt.show()
plt.savefig('bml/sawtooth_data.pdf', bbox_inches='tight')
plt.close()

fig, ax = plt.subplots(figsize=(12, 8.5))

pop_pakistan = [2.019,
                2.433,
                3.831,
                6.819

                ]
pop_india = [30.837,
             76.881,
             165.565,
             380.831
             ]

plt.plot(year, pop_india, "-go")
plt.plot(year, pop_pakistan, "-go")
plt.plot(year, pop_pakistan, label='Query')
plt.plot(year, pop_india, label="Write")
# plt.xlabel('Number of thread')
plt.xlabel('Number of record data', fontsize=25)
plt.ylabel('Time(s)', fontsize=25)
# # plt.title('Pakistan India Population till 2010')
plt.legend(prop={'size': 25})
plt.ylim(0, 500)
plt.tick_params(labelsize=25)

# plt.xlim(1000, 10000)

for index, data in enumerate(pop_pakistan):
    flag = 0
    if index == len(pop_india) - 1:
        flag = 100
    plt.text(x=year[index] - 270 - flag, y=data + 6, s=f"{data}", fontdict=dict(fontsize=22))
# plt.text(x=year[4]-9, y=pop_pakistan[4] + 5, s=f"{pop_pakistan[4]}", fontdict=dict(fontsize=15))
for index, data in enumerate(pop_india):
    flag = 0
    flag1 = 0
    if index >= len(pop_india) - 2:
        flag = 10
        flag1 = 430
    plt.text(x=year[index] - 450 - flag - flag1, y=data + 20 - flag, s=f"{data}", fontdict=dict(fontsize=22))
# plt.text(x=year[4] - 0.1, y=pop_india[4] + 5, s=f"{pop_india[4]}", fontdict=dict(fontsize=15))

plt.tight_layout()

plt.savefig('bml/eth_data.pdf', bbox_inches='tight')
# plt.show()
# plt.savefig('bml/line_plot.pdf')
