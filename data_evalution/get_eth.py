import time
import threading
import os
from bs4 import BeautifulSoup
import requests

temp = [{'address': '0xDf929Ba62cDE34af585C6A9A9e134734b842aFFB',
         'private_key': '0x1de56220f2a5dbf539cf9af65de11781a8fca5e8f28086966cbd9b5a93028b07'},
        {'address': '0x66CEa0231BEe6f7Cbb193D64DFBAfB7bFd586817',
         'private_key': '0x1a9c839b350c8b5e8b6cc15d226fbfb00e01bc1437d70697e44b8ceb430a4eef'},
        {'address': '0x16ccE32fD247A052b612025AcacAE1BE9Ac20Af5',
         'private_key': '0xc8099ce4f33709f4e77ba45d776beb76fc43fd54c33bb4791f784e51f92c9813'},
        {'address': '0x5D8EeD0c52761ca69Bd1cFB534C7B457Fb9f3056',
         'private_key': '0x104d2c2ee3a2fcbcebd14e1dccb69d210226d55228fdf84947b9707c92cc50b7'},
        {'address': '0x5637a6b28030D741CCb1EC55e2483850042459b5',
         'private_key': '0xc5246a4f6e964e554ab2d02dfbf6c1246ce928d4ad409eb9d7eb72c9441f74d0'},
        {'address': '0x25B87412f7e7F33d62e1448d940BEfd2195c0240',
         'private_key': '0xf20473d019cee2a7ff300d6c18725aa43ad8676595249968bdfcd5fcf3e600b7'},
        {'address': '0x281844e5BC9f9FAc01bbe39016C279843cC540fC',
         'private_key': '0xe1d2e0b45d7542fa38101c2bfc540217f22c2f3b1fdee876d419f52ffc6cdb99'},
        {'address': '0xE425D45A91597B54473e1383a12D0D1a2ECeb87e',
         'private_key': '0x54e3b1cbb6786d2b7ef52fb33d6d1a5b616ad533d332bc9157696e721ecc749a'},
        {'address': '0x38E810852721353161c0b08a0aF91a767F1D15fe',
         'private_key': '0x1d8ba6c6fddc55e98662dbce67d261bbfe5de932fbc28aeac5f815f020dfee60'},
        {'address': '0xc5B02FbD9776408A0dcCC6e105C74C9e37B469dd',
         'private_key': '0x885f76cb060773f5e6744f957386030a7b48e9f8e51134789d8c0bfea4ae2afa'},
        {'address': '0x68AFbbebC596EB8db67b95AFdDc4b6cd6F1c18Dd',
         'private_key': '0x0204fd531bc1621b5542e79643ff7488e6e45aa2e7863179b32bfb853cb13229'},
        {'address': '0xa09b20f6F738B45157b919fb7bCE7dD42Be936E0',
         'private_key': '0x349c79e579a4b4fecc057a21c27943e7af15c9b7f6150b4db4ac74157801418b'},
        {'address': '0xf5fFb5F38e345cEB54234729796A954d9b5DC27A',
         'private_key': '0x87af84af307dd5d5bddfbfc0a752306c73e9d269a00adb35bdf5fdbfd8f79361'},
        {'address': '0x30fbD3F20787bFa2A0f9B93f35752b5445ecCa54',
         'private_key': '0xa2d376970b36b386752fbdd019e1f6eb4911103933f740b1f19956e4cb84dc39'},
        {'address': '0x869aa661fB9596fA87669EB37A19225BAA44676b',
         'private_key': '0xca6011ad1e26f2a2d9e0a5de7348287f6c95d7709f1c2759629b96a338a6c6c6'},
        {'address': '0xc68fc8e52Fa57F07f2E5f46276d671E6BaFb9376',
         'private_key': '0xe868051d8524836fadc65cdf04f865195afbf1f11a89b9819fd8e1de6af8a7c4'},
        {'address': '0xDc4aC2c17aeFB1F911649a5Ef23c800ce496535B',
         'private_key': '0x7954ba5f5f58ed9cbe18bcf062af51b0ac69adb888dcb211b0e52b1b5ba2595e'},
        {'address': '0x4c7dBe1ff9DE6E5d127043D571121E4ccF53878F',
         'private_key': '0x764fb2f8a09d15320b2306c8d5b080effb3b5ee69a52839d2d5c7e94b447ad77'},
        {'address': '0x5111F682DCF2Cc99cdB7A114018bd56B98E92151',
         'private_key': '0x51b41582271291e5b945b686587bb48e676ba4bef5f515cb91b3adeb893396c2'},
        {'address': '0x6a7788c140f8628889332F766F442Db6FC77d657',
         'private_key': '0xaa712200f2e01b058e75bdce80393741c171d041c00b55710252360d391dc866'},
        {'address': '0x164C96e14fcE08D4647060B07314C7747E57C421',
         'private_key': '0x96bff386747d2e354d58e7daf59a83954687c1fa409985cbc7092a6e0777bf3e'},
        {'address': '0x6095F81156ccA4610Eb35F9224Db624a1B03EB13',
         'private_key': '0xe129c212af65391342ec2af04421220f4c44a64dc2ebbc22bacab333d4a38837'},
        {'address': '0x7f56A59b14EC037b775f77e3cAf01b1988113401',
         'private_key': '0x071459ee6d2250e6987b5b92329d4c93446e0fd216862ecad9d03f2d6541b15a'},
        {'address': '0xc7304FF343c323B14e98dDeDCe8511B839A1cfEB',
         'private_key': '0xb679c9c917d835fabd545aee16ff3c03602d1ec25f72797eba7d8d207cc9ebc7'},
        {'address': '0xDbeB09bf86543E841E94D7EA801c621663eBC883',
         'private_key': '0x4b2060798d14c494e99864c2bfcf9f4a1d022453dd89a6dbbed56c15b25589ed'},
        {'address': '0xCc5E9B8f19432405FdaD052aDc92A35878097E05',
         'private_key': '0x927a710ec5fd106416f3e4e75f519c1c70e376d0c70df517ce9fdc968058eeda'},
        {'address': '0xb55729c5986a9bdC2FaED81b68F4C77e067697eC',
         'private_key': '0x21662308a337e6b5dd798030b334cf585dec9afd555b7fb3e9dbced79b9d24d9'},
        {'address': '0xd7D37030Ed5E71Cd5B2146C22fa0017A89ea128F',
         'private_key': '0x3427615e3e32de26b3d7b970013a3adfd86d66a7444676621a07f2a0e6a70419'},
        {'address': '0x8A0a99e4F1B1B83952fdd7ec083D94782Cb69B4a',
         'private_key': '0x4977934fcafda11804588ca20a603f275ff3d06c799c24272acb8964be3f0d10'},
        {'address': '0xD8d4D0cb807ED7FCBABC416b0De811d67ec4204C',
         'private_key': '0x28569fc52ec5c5684248d861b23569bd502cca27f50b9a2c26c8a9397882fc61'},
        {'address': '0x503F413a9Ee83BF4eAA4671019232Ead54f08a39',
         'private_key': '0xb5e9ea53a4ca90c1440b640c1571b44e8a8d1544069ef872636385d19e054d1c'},
        {'address': '0x864A3a6cFED50633A548488333586482EbB2c975',
         'private_key': '0x5ed2a5bee2994eae45c9ea58590a429edfb0706ccadd67bc7040b3afbd1232fd'},
        {'address': '0x648E8d2635a07edbAFC70cF47C877Bf8DF2a5a59',
         'private_key': '0x5e2f9bcc2c7286095f31d68b0ab82480b5bac6ecf2c9b6087e9fb8d4e3d623b7'},
        {'address': '0x9513c3AeEE3dEF391EE777a27130abB03b6559E1',
         'private_key': '0x6e3647bdb9980fa0e0aec07dd77b51e70d7ff5f2e67dc39cd8d6ce65ab414c19'},
        {'address': '0x72B38150131a34Af040733058DE318388a9f8ea0',
         'private_key': '0x6733bcc5bab0e7f128f0240bedeeddf667b16ddd3cf9aa4f723a5a32dfc8dc07'},
        {'address': '0x59cb79bA17FB6CE1587828D50896499bBF800DBa',
         'private_key': '0xa6366bf4ac6e3cb6db10c6a5081de15dc40e2a03683bf4b0ff2d349aa7d27a24'},
        {'address': '0x5272B95304a3Dd161E70A6EF43Daf45b493D4fE8',
         'private_key': '0x85aa82b492e1fd38d89c53cb0407a8d80fc01cb00dd262e8d5b3c2e8b4a2790d'},
        {'address': '0xf7E132c7af864E7068A457b44756f31402a1D5a6',
         'private_key': '0x2fff765aa308c220c47ea18b40cbdd65b3cdb84f1c2aeb3c6c954b682ceada07'},
        {'address': '0x0E91E7df5EF197cd94907FDC205D9A156775B6b6',
         'private_key': '0xd2eb480cb9304689f2896b3b13e5188028296bd0af286beca4273dd55cd74615'},
        {'address': '0xD7E34FA4226F976957726A5B5088d3C63e818bDA',
         'private_key': '0xe79d32b11a1b2fd8b621e10c4e882f6fa2ce3183cae57bec8eaaf831aa88e78f'},
        {'address': '0xf5B472EE25456f3Da1F17046eE198eb38e6B2D0f',
         'private_key': '0x359df53d1969fce31b6a209e83b6d2be64e49da1d96d20fdcc233d22ece8b05c'},
        {'address': '0x41921e7187246d5C6AA890f485f041E5949f6Ef8',
         'private_key': '0xf9b9b4cbbf31780129cc3e4a5c31ea084dd4ff61f2930f73b7d5c5ee7638320b'},
        {'address': '0x15b77C83B0760ab61da939aC6aC6774C9011034C',
         'private_key': '0xecbda1b35c0707cf0b8f8186e383a54391c365ab7965c81fd34eac163446f764'},
        {'address': '0xB2dD71DcA391492CF752dacC5D043C7B3d482553',
         'private_key': '0x938542bfc417e9a35fbdd3a7dcafd2e07000e1ee46b1a983f7b1f226684d9f14'},
        {'address': '0xa6f09EFB9Bd8650Bd53FDBD4dfD5AB42D4ad1c6d',
         'private_key': '0xbaf089d0f64c51020a0f6a07f598b1708e0f7a276005f0d7a29a0c99adf6c8d8'},
        {'address': '0xc95DE79ca864dA83576057FE63a8E22142B5DB4A',
         'private_key': '0x82556d2fd76ce2ac466fed4d86a96f833a543d8947617a07fc60f5ee24ad63ee'},
        {'address': '0xa69C10B9D6AF34cE762D45B9A9C9cFE3BF6c59F5',
         'private_key': '0x89ec90886d3b8d9aee33a32d9fc7a1a4436cf908f7c79a4a97fc69c587d7058e'},
        {'address': '0x1088e5dde8c34d895CB2e10f9E1f213933Bd2E19',
         'private_key': '0xe6e198c7792fb2b708c31b0cd413e8774e1d20c1b798329044bbb54835516044'},
        {'address': '0xF6ED36968f8eA7A2E521c92bFCAa4FD0A1463C37',
         'private_key': '0x30a1de4f4fcaf5b318793ed5ad5ff54e817b668d889062c9d0df480725d48c4c'},
        {'address': '0x85c4C3f00245E21B722C1cB75Ac0f327d69A3c35',
         'private_key': '0x2e8ca6db720d687f49d5051dbdeb51d87592156435a3d4ab15540dd95051bb99'},
        {'address': '0xA965680BE8A95BA40FF978eb6f0FD23a239EC070',
         'private_key': '0x46eb86418ef241c4e3ea0276373967448ecca835048c64fc1cc5159c1b9a7e49'},
        {'address': '0xaDDe1656BD0258f68F1082070EE634A5DdE2eB9F',
         'private_key': '0x00316afb853fa4c464868fa9e254ceeb10f93bab89f4071dbd3241c87a00f306'},
        {'address': '0x8DDE193a1AEE90802603785559Ac434c3375FA04',
         'private_key': '0xb37e36273a7b4f586d5ecfb33b2145024ef9bc7de10bbca5c816a553ddbf82a3'},
        {'address': '0xA63FdfefDf660EC44dCE76dFeaD783c6eB8EfC80',
         'private_key': '0xa00c42b63bf0d3a3ac3bfa6a822ad83bbd994e15aa1d5f7e3243eb66a03b45e8'},
        {'address': '0xAc325ae72f5171FABe8A0a5Dfcc1823298970418',
         'private_key': '0xcc5f40eb091ecc1629f4ae30bb3e4257ef588fc61d845b313a6cd0a2e884889a'},
        {'address': '0xC56d3E090Fc4B30f5156Fc0e491b0292AA372DFA',
         'private_key': '0x9372e087bb95a7ef51229289ae0e9828f14055a2a2b36f88e59eda197fbc259f'},
        {'address': '0x17846eE3288CD3c09Ab987dbB760278aC7752E15',
         'private_key': '0x364a346ec2a0c8e88f42c5d14722c50d6138eccd4e292c6ab26eeb2558f66b5a'},
        {'address': '0xD92040609cF3d44E2182706553dD9b04f048915F',
         'private_key': '0xde221a6fd3c5da40ddd84706d7f05dfa4fe30e648b564971d0af361317b4fae6'},
        {'address': '0x710f2006ac0E5faEBDcaF52595FE66D8a23E7628',
         'private_key': '0x69bc7b24c872d6923ff221ceca50fac87e5349eff2f29b64dc750f69be191931'},
        {'address': '0x6db8BBC45241c7840F9133F0851662A2E888ddc7',
         'private_key': '0x40544d98a2b0c40a879e20b8db18fd2d25f07652acb2c34a7bffb58322a3d1aa'},
        {'address': '0xB685946e73e11B75f9A38a0F051F6C05aA11AC81',
         'private_key': '0x60abd9494d6f4c605ab2ba09da03f99cd7b58a457330d30f782d06da093e89f7'},
        {'address': '0xBE5a1B9ec5d0c09543aD4853539eB525fEC516d7',
         'private_key': '0x66501bf1b88221156c36255c8d4cc041ce12e0d968e39f4f3665e301e2a6348c'},
        {'address': '0x33004AB3344e96C164ea559EDb5332426E6a8dc1',
         'private_key': '0x723e35e5c93a69afdc3f29daaae7f4a520d7d871fa2c817e61b48e8757f0151b'},
        {'address': '0x364f5543a88C05603877F8E37F0c5da408697950',
         'private_key': '0xedcd3ef3a0bde2fe18da027958479620df30059a8012930531817a1b2894b5e7'}]

from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

# ua = UserAgent()  # From here we generate a random user agent
proxies = []  # Will contain proxies [ip, port]

html_thang = """
<tbody><tr role="row" class="odd"><td>41.190.147.158</td><td>49088</td><td>MU</td><td class="hm">Mauritius</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>176.36.89.203</td><td>53325</td><td>UA</td><td class="hm">Ukraine</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>118.174.233.41</td><td>41469</td><td>TH</td><td class="hm">Thailand</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>176.119.134.118</td><td>23500</td><td>ES</td><td class="hm">Spain</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>202.137.10.180</td><td>48187</td><td>ID</td><td class="hm">Indonesia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>103.78.213.226</td><td>45075</td><td>ID</td><td class="hm">Indonesia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>59.91.121.161</td><td>51445</td><td>IN</td><td class="hm">India</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>136.25.2.43</td><td>40614</td><td>US</td><td class="hm">United States</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>202.138.236.35</td><td>31173</td><td>ID</td><td class="hm">Indonesia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>188.6.197.119</td><td>30742</td><td>HU</td><td class="hm">Hungary</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>217.150.77.31</td><td>53281</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>139.5.237.163</td><td>35411</td><td>IN</td><td class="hm">India</td><td>anonymous</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>95.168.96.42</td><td>34273</td><td>HR</td><td class="hm">Croatia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>176.62.188.158</td><td>56351</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>187.45.13.190</td><td>54963</td><td>BR</td><td class="hm">Brazil</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>182.160.117.130</td><td>53281</td><td>BD</td><td class="hm">Bangladesh</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>181.113.225.198</td><td>53281</td><td>EC</td><td class="hm">Ecuador</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>103.217.173.210</td><td>53905</td><td>ID</td><td class="hm">Indonesia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>118.175.93.94</td><td>45175</td><td>TH</td><td class="hm">Thailand</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>109.245.239.125</td><td>55311</td><td>RS</td><td class="hm">Serbia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>223.165.1.170</td><td>43305</td><td>BD</td><td class="hm">Bangladesh</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>168.194.14.126</td><td>8080</td><td>BR</td><td class="hm">Brazil</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>118.172.201.32</td><td>53525</td><td>TH</td><td class="hm">Thailand</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>103.36.126.14</td><td>43999</td><td>IN</td><td class="hm">India</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>103.209.178.208</td><td>30004</td><td>IN</td><td class="hm">India</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>170.82.73.140</td><td>53281</td><td>BR</td><td class="hm">Brazil</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>164.77.147.93</td><td>53281</td><td>CL</td><td class="hm">Chile</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>41.217.217.60</td><td>39440</td><td>MW</td><td class="hm">Malawi</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>179.27.83.222</td><td>54260</td><td>UY</td><td class="hm">Uruguay</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>82.200.233.4</td><td>3128</td><td>KZ</td><td class="hm">Kazakhstan</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>170.245.64.26</td><td>45262</td><td>BR</td><td class="hm">Brazil</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>185.104.252.9</td><td>9090</td><td>LB</td><td class="hm">Lebanon</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>185.32.165.78</td><td>8080</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>36.37.113.4</td><td>36485</td><td>ID</td><td class="hm">Indonesia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>103.22.172.49</td><td>59458</td><td>IN</td><td class="hm">India</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>1.20.99.89</td><td>32963</td><td>TH</td><td class="hm">Thailand</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>195.69.218.198</td><td>33503</td><td>RU</td><td class="hm">Russian Federation</td><td>anonymous</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>176.62.185.72</td><td>45277</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>80.51.31.201</td><td>8080</td><td>PL</td><td class="hm">Poland</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>46.173.105.151</td><td>41258</td><td>UA</td><td class="hm">Ukraine</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>182.52.51.47</td><td>41146</td><td>TH</td><td class="hm">Thailand</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>216.198.188.26</td><td>51068</td><td>US</td><td class="hm">United States</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>125.26.99.185</td><td>41164</td><td>TH</td><td class="hm">Thailand</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>212.115.224.71</td><td>53281</td><td>UA</td><td class="hm">Ukraine</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>91.135.148.198</td><td>59384</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>119.82.253.175</td><td>31500</td><td>KH</td><td class="hm">Cambodia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>41.169.78.42</td><td>49848</td><td>ZA</td><td class="hm">South Africa</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>193.86.25.225</td><td>53875</td><td>CZ</td><td class="hm">Czech Republic</td><td>anonymous</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>46.39.245.204</td><td>54108</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>213.154.0.120</td><td>53281</td><td>AZ</td><td class="hm">Azerbaijan</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>78.29.42.10</td><td>4550</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>103.216.82.209</td><td>54806</td><td>IN</td><td class="hm">India</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>213.16.81.189</td><td>54040</td><td>HU</td><td class="hm">Hungary</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>193.193.240.34</td><td>45944</td><td>KZ</td><td class="hm">Kazakhstan</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>45.228.48.4</td><td>53281</td><td>BR</td><td class="hm">Brazil</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>210.48.204.134</td><td>54085</td><td>MY</td><td class="hm">Malaysia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>185.122.104.171</td><td>53281</td><td>PL</td><td class="hm">Poland</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>202.150.143.59</td><td>43921</td><td>ID</td><td class="hm">Indonesia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>31.192.248.7</td><td>53281</td><td>KG</td><td class="hm">Kyrgyzstan</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>212.56.203.114</td><td>30640</td><td>MD</td><td class="hm">Moldova, Republic of</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>194.181.82.190</td><td>35447</td><td>PL</td><td class="hm">Poland</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>188.163.170.130</td><td>41209</td><td>UA</td><td class="hm">Ukraine</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>41.139.9.47</td><td>8080</td><td>GH</td><td class="hm">Ghana</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>83.171.98.129</td><td>37819</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>146.88.23.64</td><td>53281</td><td>AU</td><td class="hm">Australia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>123.200.31.226</td><td>53281</td><td>BD</td><td class="hm">Bangladesh</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>181.211.245.74</td><td>44267</td><td>EC</td><td class="hm">Ecuador</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>74.59.132.126</td><td>49073</td><td>CA</td><td class="hm">Canada</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>80.240.115.254</td><td>36539</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>176.113.126.127</td><td>39921</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>201.86.99.130</td><td>39900</td><td>BR</td><td class="hm">Brazil</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>118.173.232.170</td><td>44029</td><td>TH</td><td class="hm">Thailand</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="odd"><td>138.255.32.65</td><td>34641</td><td>BR</td><td class="hm">Brazil</td><td>elite proxy</td><td class="hm">no</td><td class="hx">yes</td><td class="hm">1 minute ago</td></tr><tr role="row" class="even"><td>47.52.207.161</td><td>1080</td><td>HK</td><td class="hm">Hong Kong</td><td>anonymous</td><td class="hm">no</td><td class="hx">no</td><td class="hm">2 minutes ago</td></tr><tr role="row" class="odd"><td>94.141.244.39</td><td>59823</td><td>RU</td><td class="hm">Russian Federation</td><td>elite proxy</td><td class="hm">no</td><td class="hx">no</td><td class="hm">2 minutes ago</td></tr><tr role="row" class="even"><td>190.211.151.15</td><td>8080</td><td>AR</td><td class="hm">Argentina</td><td>anonymous</td><td class="hm">no</td><td class="hx">no</td><td class="hm">2 minutes ago</td></tr><tr role="row" class="odd"><td>175.100.16.20</td><td>37725</td><td>KH</td><td class="hm">Cambodia</td><td>elite proxy</td><td class="hm">no</td><td class="hx">no</td><td class="hm">2 minutes ago</td></tr><tr role="row" class="even"><td>189.43.68.174</td><td>80</td><td>BR</td><td class="hm">Brazil</td><td>anonymous</td><td class="hm">no</td><td class="hx">no</td><td class="hm">2 minutes ago</td></tr><tr role="row" class="odd"><td>87.247.3.234</td><td>52098</td><td>KZ</td><td class="hm">Kazakhstan</td><td>elite proxy</td><td class="hm">no</td><td class="hx">no</td><td class="hm">2 minutes ago</td></tr><tr role="row" class="even"><td>187.94.31.237</td><td>33296</td><td>BR</td><td class="hm">Brazil</td><td>elite proxy</td><td class="hm">no</td><td class="hx">no</td><td class="hm">2 minutes ago</td></tr></tbody>
"""
soup = BeautifulSoup(html_thang, 'html.parser')
for i in range(0, len(soup.find_all(attrs={"role": "row"}))):
    proxies.append({
        "http": "http://" + str(soup.find_all(attrs={"role": "row"})[i].next.next) + ":" + str(
            soup.find_all(attrs={"role": "row"})[i].next.next_sibling.next),
        "https": "http://" + str(soup.find_all(attrs={"role": "row"})[i].next.next) + ":" + str(
            soup.find_all(attrs={"role": "row"})[i].next.next_sibling.next),
    })


# Main function
def main():
    for i in range(0, 4):
        a = threading.Thread(target=get_eth, args=(proxies[i * 20:i * 20 + 20], temp[i * 8 + 32: i * 8 + 8 + 32]))
        a.start()


def get_eth(proxies_temp, account_temp):
    proxies_index = 0
    n = 0
    while n < len(account_temp) and proxies_index < len(proxies_temp):  # def main():
        #     for i in range(0, 4):
        #         a = threading.Thread(target=get_eth, args=(proxies[i * 20:i * 20 + 20], temp[i * 8: i * 8 + 8]))
        #         a.start()

        # def get_eth(proxies_temp, account_temp):
        #     proxies_index = 0
        #     n = 0
        #     while n < len(account_temp) and proxies_index < len(proxies_temp):
        #         try:
        #             time.sleep(10)
        #             print(account_temp[n]['address'])
        #             url = 'https://faucet.ropsten.be/donate/' + account_temp[n]['address']
        #             response = requests.get(url, proxies=proxies_temp[proxies_index])
        #             proxies_index += 1
        #             n += 1
        #             print(response.content)
        #         except:
        #             print("cannot connect to .....")
        #             proxies_index += 1
        #
        #
        # main()

        try:
            time.sleep(10)
            print(account_temp[n]['address'])
            url = 'https://faucet.ropsten.be/donate/' + account_temp[n]['address']
            response = requests.get(url, proxies=proxies_temp[proxies_index])
            proxies_index += 1
            n += 1
            print(response.content)
        except:
            print("cannot connect to .....")
            proxies_index += 1


main()

# from web3 import Web3
#
# #
# infura_url = "https://ropsten.infura.io/v3/02d9efb46d46467fbeb995c62301fc4a"
# web3 = Web3(Web3.HTTPProvider(infura_url))
# temp1 = []
# for i in range(0, len(temp)):
#     balance = web3.eth.getBalance(temp[i]['address']) / 1000000000000000000
#     if balance > 0.0:
#         temp1.append(temp[i])
#     print(temp[i]['address'] + "   " + str(balance))
# print(temp1)
# # 0xa14056D141EDA673E64643Bbb84cf158bb13Ef52
