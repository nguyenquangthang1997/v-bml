import pandas as pd
import dicttoxml
import json
import pymysql
from elasticsearch import Elasticsearch

# def read_to_dict(path):
xlsx_dict = []
datas = pd.read_excel("data_paper_1000.xlsx", sheet_name="Sheet1")
row = datas.shape[0]
for i in range(0, row):
    temp = {
        "Id": int(datas['Id'][i]),
        "name": str(datas['name'][i]),
        "age": str(datas['age'][i]),
        "address": str(datas['address'][i]),
        "long": str(datas['long'][i]),
        "height": str(datas['height'][i]),
        "weight": str(datas['weight'][i]),
        "phone": str(datas['phone'][i]),
        "email": str(datas['email'][i]),
    }
    xlsx_dict.append(temp)

# # write xml
# xml = dicttoxml.dicttoxml(xlsx_dict)
# file = open("data_paper_1000.xml", "w")
# file.write(xml.decode("utf-8"))
# file.close()
#
# # write json
# dict_data = []
# for i in range(0, 5000):
#     temp = {
#         "Id": i,
#         "name": "thang"+str(i),
#         "age": str(i),
#         "address": "haiduong"+str(i),
#         "long": str(i),
#         "height": str(i),
#         "weight": str(i),
#         "phone": str(i),
#         "email": str("nguyenquangthang1@gmail.com"),
#     }
#     dict_data.append(temp)
# json_string = json.dumps(dict_data)
# file = open("data_paper_5000.json", "w")
# file.write(json_string)
# file.close()

# # save database
#
# import mysql.connector
#
# mydb = mysql.connector.connect(
#     host="localhost",
#     user="thang",
#     passwd="nqt760847",
#     database="thang"
# )
#
# db = pymysql.connect("localhost", "thang", "nqt760847", "thang")
# cursor = db.cursor()
# attributes = ['Id', 'name', 'age', 'address', 'long', 'height', 'weight', 'phone', 'email']
# # sql = "CREATE TABLE data_paper(`Id` int, `name` varchar(255), `age` varchar(255), `address` varchar(255), `long` varchar(255), `height` varchar(255), `weight` varchar(255), `phone` varchar(255), `email` varchar(255));"
# # cursor.execute(sql)
#
# mycursor = mydb.cursor()
#
# sql = "Insert into  data_paper(`Id`,`name`,`age`,`address`,`long`,`height`,`weight`,`phone`,`email`) values(%s, %s,%s,%s,%s,%s,%s,%s,%s)"
# val = [tuple(item.values()) for item in xlsx_dict]
#
# mycursor.executemany(sql, val)
#
# mydb.commit()

# # save elasticsearch
es = Elasticsearch([{"host": "localhost", "port": "9201"}])

for i in range(0, len(xlsx_dict)):
    body = xlsx_dict[i]
    es.index(index="data_paper", body=body)
