# datablockchain

Start nodes validator:
 
```
cd save_private_blockchain/network/bml
docker-compose up
```

Start sawtooth api

```
cd save_private_blockchain/vbml_sawtooth
docker-compose up
```

Start V-BML system

```
cd api
python3 main.py
```

