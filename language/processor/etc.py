import json
import pandas as pd
import io
import xmltodict
from elasticsearch import Elasticsearch
import pymysql
import math
import os
from datetime import datetime
import time
import sys

import requests

sys.path.append("../../api")
import redis_controller as res
import elastic as elasticsearch


def processor(account, mapping):
    if mapping['source']['data_type'] == "JSON":
        return json_processor(account=account, mapping=mapping)
    elif mapping['source']['data_type'] == "XLSX":
        return xlsx_processor(account=account, mapping=mapping)
    elif mapping['source']['data_type'] == "XML":
        return xml_processor(account=account, mapping=mapping)
    elif mapping['source']['data_type'] == "SQL":
        return sql_processor(account=account, mapping=mapping)
    elif mapping['source']['data_type'] == "ELASTIC_SEARCH":
        return elastic_processor(account=account, mapping=mapping)
    else:
        return {}


def xlsx_processor(account, mapping):
    mapping = pre_processing(account=account, mapping=mapping)
    data_to_save = []
    record_temp = {}
    iterator = mapping['source']['iterator']
    path = mapping['source']['path']
    res = requests.get(path)
    toread = io.BytesIO()
    toread.write(res.content)
    toread.seek(0)
    datas = pd.read_excel(toread, sheet_name=iterator)
    row = datas.shape[0]
    for i in range(0, row):
        for j in range(0, len(mapping['data_save'])):
            record_temp[mapping['data_save'][j]] = str(datas[mapping['data_source'][j]][i])
        data_to_save.append(record_temp.copy())
    data_to_save = json_primary_linking(account=account, mapping=mapping, data_to_save=data_to_save)
    # os.system("rm " + path)
    return data_to_save


def json_processor(account, mapping):
    mapping = pre_processing(account=account, mapping=mapping)
    data_to_save = []
    record_temp = {}
    # mapping = json.loads(mapping)
    path = mapping['source']['path']
    res = requests.get(path)
    datas = json.loads(res.content.decode())

    iterator_array = mapping['source']['iterator'].split(".")
    if len(iterator_array) == 1 and iterator_array[0] == '':
        pass
    else:
        for iterator in iterator_array:
            datas = datas[iterator]
    for data in datas:
        for i in range(0, len(mapping['data_save'])):
            attribute_save_index = mapping['data_save'][i]
            attribute_save_index_array = attribute_save_index.split(".")
            attribute_source_index = mapping['data_source'][i]
            attribute_source_index_array = attribute_source_index.split(".")
            attribute_source_value = data
            for j in range(0, len(attribute_source_index_array)):
                attribute_source_value = attribute_source_value[attribute_source_index_array[j]]
            cursor = record_temp
            for j in range(0, len(attribute_save_index_array) - 1):
                if attribute_save_index_array[j] not in record_temp:
                    cursor[attribute_save_index_array[j]] = {}
                cursor = cursor[attribute_save_index_array[j]]
            cursor[attribute_save_index_array[-1]] = attribute_source_value
        data_to_save.append(record_temp.copy())
    data_to_save = json_primary_linking(account=account, mapping=mapping, data_to_save=data_to_save)
    return data_to_save


def xml_processor(account, mapping):
    mapping = pre_processing(account=account, mapping=mapping)
    data_to_save = []
    record_temp = {}
    # mapping = json.loads(mapping)
    path = mapping['source']['path']
    res = requests.get(path)
    datas = xmltodict.parse(res.content.decode())

    iterator_array = mapping['source']['iterator'].split(".")
    if len(iterator_array) == 1 and iterator_array[0] == '':
        pass
    else:
        for iterator in iterator_array:
            datas = datas[iterator]

    for data in datas:
        for i in range(0, len(mapping['data_save'])):
            attribute_save_index = mapping['data_save'][i]
            attribute_save_index_array = attribute_save_index.split(".")
            attribute_source_index = mapping['data_source'][i]
            attribute_source_index_array = attribute_source_index.split(".")
            attribute_source_value = data
            for j in range(0, len(attribute_source_index_array)):
                attribute_source_value = attribute_source_value[attribute_source_index_array[j]]
            cursor = record_temp
            for j in range(0, len(attribute_save_index_array) - 1):
                if attribute_save_index_array[j] not in record_temp:
                    cursor[attribute_save_index_array[j]] = {}
                cursor = cursor[attribute_save_index_array[j]]
            cursor[attribute_save_index_array[-1]] = attribute_source_value
        data_to_save.append(record_temp.copy())
    data_to_save = json_primary_linking(account=account, mapping=mapping, data_to_save=data_to_save)
    # os.system("rm " + path)
    return data_to_save


def elastic_processor(account, mapping):
    mapping = pre_processing(account=account, mapping=mapping)
    data_to_save = []
    record_temp = {}
    path = mapping['source']['path'].split("/")
    es = Elasticsearch([{'host': path[0], 'port': path[1]}])
    # res = es.search(index='user', body={"size": 100, "query": {"match_all": {}}})
    res = es.search(index=mapping['source']['iterator'], body={"query": {"match_all": {}}, "size": 10000000})
    res = res['hits']['hits']
    if len(res) > 0:
        for data in res:
            for i in range(0, len(mapping['data_save'])):
                attribute_save_index = mapping['data_save'][i]
                attribute_save_index_array = attribute_save_index.split(".")
                attribute_source_index = mapping['data_source'][i]
                attribute_source_index_array = attribute_source_index.split(".")
                attribute_source_value = data["_source"]
                for j in range(0, len(attribute_source_index_array)):
                    attribute_source_value = attribute_source_value[attribute_source_index_array[j]]
                cursor = record_temp
                for j in range(0, len(attribute_save_index_array) - 1):
                    if attribute_save_index_array[j] not in record_temp:
                        cursor[attribute_save_index_array[j]] = {}
                    cursor = cursor[attribute_save_index_array[j]]
                cursor[attribute_save_index_array[-1]] = attribute_source_value
            data_to_save.append(record_temp.copy())
        # data_to_save = json.dumps(data_to_save)
        data_to_save = json_primary_linking(account=account, mapping=mapping, data_to_save=data_to_save)
    return data_to_save


def sql_processor(account, mapping):
    mapping = pre_processing(account=account, mapping=mapping)
    data_to_save = []
    record_temp = {}
    path = mapping['source']['path'].split("/")
    db = pymysql.connect(path[0], path[1], path[2], path[3])
    cursor = db.cursor()
    sql = "SELECT "
    for i in range(0, len(mapping['data_source'])):
        sql += "`" + mapping['data_source'][i] + "` "
        if i != len(mapping['data_source']) - 1:
            sql += ","
    sql += " FROM `" + mapping['source']['iterator'] + "`"

    cursor.execute(sql)
    datas = cursor.fetchall()

    for data in datas:
        for i in range(0, len(mapping['data_save'])):
            record_temp[mapping['data_save'][i]] = data[i]
        data_to_save.append(record_temp.copy())
    data_to_save = json_primary_linking(account=account, mapping=mapping, data_to_save=data_to_save)
    return data_to_save


def join_list_object(list1, list2, attribute1, attribute2):
    result = []
    dict1 = {}
    for i in range(0, len(list1)):
        dict1[str(list1[i][attribute1])] = []
    for i in range(0, len(list2)):
        dict1[str(list2[i][attribute2])] = []
    for i in range(0, len(list1)):
        dict1[str(list1[i][attribute1])].append(i)
    for i in range(0, len(list2)):
        dict1[str(list2[i][attribute2])].append(i)
    for value in dict1.values():
        if len(value) == 2:
            object_temp = list2[value[1]]
            del object_temp[attribute2]
            result.append({**list1[value[0]], **object_temp})
    return result


def json_primary_linking(account, mapping, data_to_save):
    for interlinking in mapping['interlinking']:
        index_name = interlinking['index_name']
        primary_data = interlinking['primary_data']
        if len(primary_data) == 2:
            temp_mapping = get_index_name(account=account, index_name=index_name)
            data_to_save_temp = processor(account=account, mapping=temp_mapping)
            data_to_save = join_list_object(data_to_save, data_to_save_temp, primary_data[0], primary_data[1])
    return data_to_save


# temp
def get_index_name(account, index_name):
    return elasticsearch.get_index_name(account, index_name)


def pre_processing(account, mapping):
    data_save = mapping['reference']
    data_save_temp = []
    source_temp = []
    interlinking = []
    for attribute in data_save:
        check_interlink = check_interlinking(attribute=attribute)
        if check_interlink['code'] == 2:
            mapping_temp = get_index_name(account=account, index_name=check_interlink['id'])
            mapping_temp = pre_processing(account=account, mapping=mapping_temp)
            data_save_temp.extend(mapping_temp['reference'])
            source_temp.extend(mapping_temp['reference'])
            interlinking.extend(mapping_temp['interlinking'])
        elif check_interlink['code'] == 1:
            interlinking.append({
                "id": check_interlink["id"],
                "primary_data": check_interlink["primary_data"]
            })
        else:
            temp_array = attribute.split("->")
            data_save_temp.append(temp_array[1])
            source_temp.append(temp_array[0])
    mapping['data_save'] = data_save_temp
    mapping['data_source'] = source_temp
    mapping['interlinking'] = interlinking
    return mapping


def check_interlinking(attribute):
    first = 0
    last = 0
    for i in range(0, len(attribute)):
        if attribute[i] == "(":
            first = i
        elif attribute[i] == ")":
            last = i
            break
    if first and last:
        if last - first == 1:
            return {"code": 2,
                    "id": attribute[0:first].replace(" ", ""),
                    "primary_data": []
                    }  # linking without primary
        else:
            primary_array = attribute[first + 1: last].replace(" ", "").split(",")
            return {"code": 1,
                    "id": attribute[0:first].replace(" ", ""),
                    "primary_data": primary_array
                    }  # linking with primary
    else:
        return {"code": 0,
                "id": "",
                "primary_data": []
                }  # not linking


def transform_function(transform_function, data_to_save):
    transform_array = []
    for transform in transform_function:
        transform_array_temp = transform.replace(" ", "").split("->")
        attribute_array = transform_array_temp[0].split(".")
        str_replace = "data"
        for attribute in attribute_array:
            str_replace += "['" + attribute + "']"
        transform_array_temp[1] = transform_array_temp[1].replace(transform_array_temp[0], str_replace)
        transform_array.append(transform_array_temp)
    for data in data_to_save:
        for transform in transform_array:
            attribute_array = transform[0].split(".")
            cursor = data
            for attribute in attribute_array[:-1]:
                cursor = cursor[attribute]
            cursor[attribute_array[-1]] = eval(transform[1])
    return data_to_save


def main(mapping, account):
    data_to_save = processor(account=account, mapping=mapping)
    data_to_save = transform_function(mapping['function'], data_to_save)
    data_to_save = json.dumps({mapping['identifier']: data_to_save})
    devide_file(account=account, data=data_to_save, index_name=mapping['identifier'])


def get_time():
    dateTimeObj = datetime.now()
    return dateTimeObj.strftime("%d_%b_%Y_%H_%M_%S_%f")


def devide_file(account, data, index_name, max_length=25000):
    data = json.loads(data)
    iterator = int(max_length / sys.getsizeof(data[index_name][0]))
    list_items = [data[index_name][i:i + iterator] for i in range(0, len(data[index_name]), iterator)]

    for item in list_items:
        timestamp = get_time()
        data1 = {index_name: item}
        if not os.path.exists('data/'):
            os.makedirs('data/')
        path = "data/" + timestamp + ".json"
        with open(path, 'w') as fp:
            json.dump(data1, fp)
        res.add_mapping_synchronize_transaction(account, path)
