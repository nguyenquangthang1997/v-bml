import sys
import os
import json
from datetime import datetime

sys.path.append("..")
import etc


def get_time():
    dateTimeObj = datetime.now()
    return dateTimeObj.strftime("%d_%b_%Y_%H_%M_%S_%f")


def main(mapping, account):
    etc.main(mapping=mapping, account=account)
