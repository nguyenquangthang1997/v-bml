import os
from file import json_file_processor
from file import xlsx_file_processor
from file import xml_file_processor
from database import elasticsearch_processor
from database import sql_processor


def processor(mapping, account):
    for mapping_child in mapping:
        if mapping_child['source']['data_type'] == "JSON":
            json_file_processor.main(mapping=mapping_child, account=account)
        elif mapping_child['source']['data_type'] == "XLSX":
            xlsx_file_processor.main(mapping=mapping_child, account=account)
        elif mapping_child['source']['data_type'] == "XML":
            xml_file_processor.main(mapping=mapping_child, account=account)
        elif mapping_child['source']['data_type'] == "SQL":
            sql_processor.main(mapping=mapping_child, account=account)
        elif mapping_child['source']['data_type'] == "ELASTIC_SEARCH":
            elasticsearch_processor.main(mapping=mapping_child, account=account)
        else:
            print("data_type = is invalid")
