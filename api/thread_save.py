import time
import threading
import redis_controller as res
import contract as contract
import elastic
from config.config import Config
import os
from Crypto.Cipher import AES


def decrypt_private_key(aes_key, public_key, encrypted_private_key):
    init_vector = bytes.fromhex(public_key[:32])
    cipher = AES.new(bytes.fromhex(aes_key), AES.MODE_CBC, init_vector)
    private_key = cipher.decrypt(bytes.fromhex(encrypted_private_key))
    return private_key


def encrypt_private_key(aes_key, public_key, private_key):
    init_vector = bytes.fromhex(public_key[:32])
    cipher = AES.new(bytes.fromhex(aes_key), AES.MODE_CBC, init_vector)
    return cipher.encrypt(private_key)


def save():
    num_thread = 16
    # print("thread_save")
    account_list = res.get_all_key()
    if len(account_list):
        for account in account_list:
            time_start = time.time()

            aes_key = 'ffffffffffffffffffffffffffffffff'
            init_vector = "abcdefghiklmnooookajskaaaa"
            account_temp = account.decode("utf-8")
            if len(account) < 50:
                account = account.decode("utf-8")[2:] + "0" * 24
            else:
                account = account.decode("utf-8") + "0" * 62
            address = encrypt_private_key(aes_key,
                                          init_vector.encode().hex(),
                                          bytes.fromhex(account)).hex()
            account_data = elastic.get_account_data(address)

            account_data['encrypted_private_key'] = decrypt_private_key(aes_key,
                                                                        init_vector.encode().hex(),
                                                                        account_data['encrypted_private_key']).hex()
            account_data['account'] = account_temp
            path = []
            path_temp = res.get_mapping_synchronize_transaction(account_temp)
            while path_temp:
                path.append(path_temp.decode("utf-8"))
                path_temp = res.get_mapping_synchronize_transaction(account_temp)
            data_transaction = contract.save_data(path, account_data, num_thread)
            for path_temp in data_transaction['data_path']:
                res.add_mapping_synchronize_transaction(account_temp, path_temp)
            path_correct = [path_temp for path_temp in path if path_temp not in data_transaction['data_path']]
            for path_temp in path_correct:
                os.system("rm " + path_temp.decode("utf-8"))
            # for transaction_temp in data_transaction['transaction']:
            elastic.create_mapping_blockchain_transaction_record(address,
                                                                 data_transaction['transaction'])
            time_end = str(time.time() - time_start)
            file = open("../data_evalution/data_100/sawtooth.txt", "a+")
            file.writelines(account_data['account'] + "++s++" + time_end + "\n")
            file.close()

    threading.Timer(1, save).start()


save()
