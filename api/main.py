from aiohttp import web
import os
from handler import Handler
import aiohttp_cors
import sys
from config.config import Config
# import ssl
import logging
import thread_save

LOGGER = logging.getLogger(__name__)


def start_server():
    app = web.Application()
    # print(dir)
    handler = Handler()
    app['aes_key'] = 'ffffffffffffffffffffffffffffffff'
    app['secret_key'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    app['init_vector'] = "abcdefghiklmnooookajskaaaa"

    app.router.add_route('POST', "/synchronize", handler.synchronize)
    app.router.add_route('POST', "/create_account", handler.create_account)
    app.router.add_route('POST', "/get", handler.get_data)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    for route in list(app.router.routes()):
        cors.add(route)
    web.run_app(app=app, host=Config().host, port=Config().port)


start_server()
