from elasticsearch import Elasticsearch
import uuid
import logging

es = Elasticsearch([{"host": "178.128.217.254", "port": "9200"}])


def create_blockchain_user_index():
    if not es.indices.exists(index="blockchain_user"):
        body = {
            "mappings": {
                "properties": {
                    "account": {"type": "keyword"},
                    "encrypted_private_key": {"type": "text"},
                    "blockchain_type": {"type": "text"},
                    "endpoint_url": {"type": "text"}
                }
            }
        }
        try:
            res = es.indices.create(index="blockchain_user", body=body)
            return res
        except Exception as e:
            print("already exist")


def get_account_data(account):
    body = {
        "query": {
            "match": {
                "account": account
            }
        }
    }
    res = es.search(index='blockchain_user', body=body)

    try:
        return res['hits']['hits'][0]['_source']
    except:
        return []


def create_blockchain_record(account, encrypted_private_key, type, endpoint_url):
    body = {
        "encrypted_private_key": encrypted_private_key,
        "account": account,
        "blockchain_type": type,
        "endpoint_url": endpoint_url
    }
    res = es.index(index='blockchain_user', doc_type='_doc', body=body)
    return {"result": res['result'], "account": account}


def create_mapping_blockchain_transaction_index():
    if not es.indices.exists(index="mapping_blockchain_transaction"):
        # body = {
        #     "mappings": {
        #         "properties": {
        #             "account": {"type": "keyword"},
        #             "transaction_id": {"type": "text"}
        #         }
        #     }
        # }
        try:
            res = es.indices.create(index="mapping_blockchain_transaction")
            return res
        except Exception as e:
            print("already exist")


def fetch_transaction_id(account):
    """
    :param account:
    :return:
    """
    body = {
        "query": {
            "match": {
                "account": account
            }
        }
    }

    res = es.search(index='mapping_blockchain_transaction', body=body)
    try:
        return res['hits']['hits'][0]["_source"]['transaction_id']
    except:
        return []


def create_mapping_blockchain_transaction_record(account, transaction_id):
    body = {
        "query": {
            "match": {
                "account": account
            }
        }
    }
    res = es.search(index='mapping_blockchain_transaction', body=body)
    if len(res['hits']['hits']) == 0:
        body = {
            "transaction_id": transaction_id,
            "account": account
        }
        res = es.index(index='mapping_blockchain_transaction', doc_type='_doc', body=body)
    else:
        id = res['hits']['hits'][0]['_id']
        transaction_id_list = res['hits']['hits'][0]['_source']['transaction_id']
        transaction_id_list.extend(transaction_id)
        body = {
            "doc": {
                "transaction_id": transaction_id_list,
                "account": account
            }
        }
        res = es.update(index='mapping_blockchain_transaction', id=id, body=body)

    return {"result": res['result'], "account": account}


def create_mapping_blockchain_mapping_index():
    if not es.indices.exists(index="mapping_blockchain_mapping"):
        try:
            res = es.indices.create(index="mapping_blockchain_mapping")
            return res
        except Exception as e:
            print("already exist")


def create_mapping_blockchain_mapping_record(account, mapping):
    mapping_saved_record = fetch_mapping(account)
    if mapping_saved_record != {}:
        mapping_saveds = mapping_saved_record["_source"]['mapping']
        id = mapping_saved_record["_id"]
        for j in range(0, len(mapping)):
            flag = 0
            for i in range(0, len(mapping_saveds)):
                if mapping_saveds[i]['identifier'] == mapping[j]['identifier']:
                    mapping_saveds[i] = mapping[j]
                    flag = 1
            if flag == 0:
                mapping_saveds.append(mapping[j])
        body = {
            "doc": {
                "mapping": mapping_saveds,
                "account": account
            }
        }
        res = es.update(index='mapping_blockchain_mapping', id=id, body=body)
    else:
        body = {
            "mapping": mapping,
            "account": account
        }
        res = es.index(index='mapping_blockchain_mapping', doc_type='_doc', body=body)
    return {"result": res['result'], "account": account}


def fetch_mapping(account):
    body = {
        "query": {
            "match": {
                "account": account
            }
        }
    }
    res = es.search(index='mapping_blockchain_mapping', body=body)
    try:
        return res['hits']['hits'][0]
    except:
        return {}


def get_index_name(account, index_name):
    mapping_saved_record = fetch_mapping(account)
    if mapping_saved_record == {}:
        return {}
    else:
        mapping_saveds = mapping_saved_record["_source"]['mapping']
        for mapping_saved in mapping_saveds:
            if mapping_saved["id"] == index_name:
                return mapping_saved
        return {}


create_blockchain_user_index()
create_mapping_blockchain_transaction_index()
create_mapping_blockchain_mapping_index()
# create_mapping_synchronize_transaction_index()
