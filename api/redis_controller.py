import redis

res = redis.Redis(host='localhost', port=6379, db=0)


def add_mapping_synchronize_transaction(account, path):
    res.rpush(account, *[path])


def get_mapping_synchronize_transaction(account):
    path = res.rpop(account)
    return path


def get_all_key():
    return res.keys()


# for i in range(1, 6):
#     print(get_mapping_synchronize_transaction("0x2cEccD8e681480ee0BA4A7d1BB8BE4fFC576A3c0"))
