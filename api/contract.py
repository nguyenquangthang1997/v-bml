from web3 import Web3
from config.config import Config
import requests
import json
import elastic
import threading
import queue
import math

data_get_return = []
data_path_global = []
transaction_global = []
# account_core = [
#     {'address': '0xE9fB290e106eb7715aDD03e90334525160dC3f65',
#      'private_key': 'c07d2ffb55761c5f8c2cde2b58f0fa332cfcecf714b55ee1474156e072f3cc3f'},
#     {'address': '0xf85a79d980D891628E9D19f06aE70142dEDfC305',
#      'private_key': '9985642f8205084023ee13803faeab8c7be449b8612dc07a38509c55bbd12a9f'},
#     {'address': '0x296B511B09828CcC55b1C3fb402d71312AE550b6',
#      'private_key': '66876c60957e0a03c0e5e1d9d48fb9ae0bca44e88f74f71ecaacfe37c1936c05'},
#     {'address': '0x59ed8C0ec1361543A6E5D6c841F136d64d00a7b1',
#      'private_key': '21228650089f8bd304b31b5f24660c67c9bc678d76d682ed2a41faee8591b938'},
#     {'address': '0xc44615829F96114ffB1447e927dD5b6c87334757',
#      'private_key': 'da1a11a2d24399c913bd1dc29e224e56a33effd6ce7161ff1c9f2d4f3f6644de'},
#     {'address': '0xD6fF548292B6a3820CEaB485Eccf28c29b977647',
#      'private_key': '6c608926fe09707d4d82f9aeb9e97a559618c9b612d788e0cf7d4504f1f479ab'},
#     {'address': '0x88212284b37a1DC2b8400708B6afd63edf1CaF85',
#      'private_key': 'dbfe9f2be7bcadb82268f94aed899253ffc4a5c68d39f228ea5dc2354b594f47'},
#     {'address': '0xD5964D4AEd1cC26E48274664850576ed9e6ac9a7',
#      'private_key': '4fb8714a8bec1a6078094729848df9a0bfc6615989b2daa4412a7a93b62ffc57'},
#     {'address': '0x6Cc4BC1Bcaba471F539401E9B7ca6E2CC043245A',
#      'private_key': 'bf39dc250e742c1fd7203f7fd6e48e18d0783a63b1c25d4c49f025a22c72d96f'},
#     {'address': '0x4C2834F61Ac5c88De339aB202FAb4ebAD7338926',
#      'private_key': '8f3f4c6ca79f3851e69bcc83f1c3570b70b66f66cf062a540bc70fc8b4bdce31'},
#     {'address': '0xa14056D141EDA673E64643Bbb84cf158bb13Ef52',
#      'private_key': '06620524a1092225912ba1dd5a34003d40d271da921bd8a2ed4f2cfb18b5daf9'},
#     {'address': '0x9575D29ee95FDE64ed208008935B1A2F8eA89755',
#      'private_key': '957327083b81980e610fcb99cfb9b8c607e3d53732876a5b7b85e3a3a5d65010'},
#     {'address': '0x9f09616586B672933807a7F1e75Dc703b15e2F61',
#      'private_key': 'f94781860f719b5f39484fbeebf09a55d481e5efee7b8961086db96e22b308ec'},
#     {'address': '0x79400FEF5Ba968513E503f23fd3729B37936ADd2',
#      'private_key': '4f2fbe120d935f00c96b52d89868c4e45d5f682c716db5096327098f21a67c8c'},
#     {'address': '0xf2a506920B0F2555B8c09b779eA7C2C99253e820',
#      'private_key': 'bf65364d3c65e567049436417978840f38f248d67d057f1ecbe2e657cb40210b'},
#     {'address': '0xb8a9A4CC20d0Afd0c5428E72Cc99d24a0FfBbA29',
#      'private_key': 'b8ca6d1ec820b30845187dc26cc57222213b58b673e488e0ff9fbc625ab3e0ca'}]
# account_core = [
#     {'address': '0x575d9A887fa43f2FD9579a159b020229EB2fee15',
#      'private_key': '9e8d61547f425e48fdb7e47c2716c40ab423a8b42d20f24e31407aa7f1641d31'},
#     {'address': '0xf6BEf2535b796B2096d9c9f4D29C3Ff60305D554',
#      'private_key': '8e35a24a326c5caa481b3d2b25b571ba475df0172a897fff046ff53ec689cd2f'},
#     {'address': '0xdf756aa076eFC20B4c9A7c1115fD54E81764753F',
#      'private_key': '08d2a771aafcbda8fbf0c8d843a3910faed7e170ea20077c7c35b20f01b69104'},
#     {'address': '0xEf5cbA7b2A1e95cDac829a5A9bA11b1bBd147ED3',
#      'private_key': 'e4260ba5cb0f304e7708799e19c287677c61b3beca21d7c1e0b02f18c400ed73'},
#     {'address': '0x5e839e089fE2e934Fdf7B52E2e9284c381721522',
#      'private_key': '735f13a030ef1ad8594f4706789a94f9b7f45ae8f21dcfdb05720b1fe8e6cabd'},
#     {'address': '0x66cA1634ae2432A9BD7A984402D2D4d511F9D0AA',
#      'private_key': 'f06a476880508b647ccd1020d51891c07936f22d924ed660548a16eb3ad893d9'},
#     {'address': '0x72C6228b6FdF1259E77d6e40dED8895d0332BB78',
#      'private_key': '5fe962860de16c005ddc347ce0e6d7e4d9c897525d73faedf4de2dce0a1ac43c'},
#     {'address': '0xadD11cDE59004557f97b8dDD3203c69B46D7470C',
#      'private_key': 'f048271f4c941ef370c1cbdeca83efca662467b44df5f144d5d9ffc84a014a07'},
#     {'address': '0x10b9a66f44FBA52d6885F359e5216252349Da83d',
#      'private_key': 'fef8b9be7ae78290091931e206546e71b0bdd05dc57104684bf84100b180471b'},
#     {'address': '0x149B723D44087Ff6e585714d7aA748A20a368aC2',
#      'private_key': 'a5a2384e14ae46839b4e85a42d7508ae3829ec22b3e5106781f128cc74eb177e'},
#     {'address': '0xa36d96C6e9acE7E6089B974dB339B066ef0e53d4',
#      'private_key': 'edbd4c3a807667c65053d8ca302b068f0919d5eb1427f2b90658639e3ff1516f'},
#     {'address': '0x3FE4367Bf1cFaef7Ab64b643B4Fc58cEC5C4a98f',
#      'private_key': '3b20f6fb77908c7a7811344b53c56867ff4e1a80485db3b4b2568db1ede400ad'},
#     {'address': '0xcF8053c07f4FaaB2cd128Cd54d05fA5eF064990F',
#      'private_key': 'f334952a3085e516c1fc7e3e0b00c6816471580fcb775e58a73dd0df937b18e1'},
#     {'address': '0x635e53865F88476D918ba39B28d8e7d8Dcb5AEC7',
#      'private_key': '3e05f74d600b6b7d3497f689938880a236c6f746a4b286e5a935b70b9063d362'},
#     {'address': '0x567D754A8BfF34059E2805684c185B59bfCb8464',
#      'private_key': '8ccdbe0984726c686014104180aa794191036a96f707f7f0811a5d423c28cd7f'},
#     {'address': '0x546372571559453350E33d2fB7a5a2b8640a9508',
#      'private_key': 'ae6e2ba1a0083a6f24c6d9fcf447f4e46801b6ca457ff1b7dbb81c17984ea712'}
# ]
account_core = [{'address': '0x9F214d3844f532b451D55E32892102A3919cEb1F', 'private_key': '6e4fb4fa6e75eaa9964ac90627bc0acccf413e7244a13bedb2667ba5fdc165bc'}, {'address': '0x2A6a5f8614ACf82a670822B1cca47F98a6A00C65', 'private_key': 'aab3d48f64c09c85f2c36ab739406dd1b401fb385f45823af993f53627cf78e8'}, {'address': '0x498507A69F8799Fa12e423f02B5f50Fe5033F56E', 'private_key': 'e7d0b4766f7a9ec8dc166c91ae1ecc9915f286a249e29b1ac114bce6a3075dfe'}, {'address': '0xD04086b63F455894625D76cB9c9eB6B405Fb2e96', 'private_key': 'fa96d87c161cc7fcecc1f9ed1ec7dfd59ff728ecab736e25c5ad42a1e011accb'}, {'address': '0x654d8EBbDe406Cb5433860Bad033d231631c5625', 'private_key': '254bc93c18b912683ccdcaedca63f4ba5a79c59c9eeea11c00f6ee1c51614834'}, {'address': '0xA793dC8BF4536a24B63BF4449732bb4e206b3632', 'private_key': '06194d5aa9bd4facf4a47ce5af339fed0b2341428ea9cc96bd55e6fbe10ddda9'}, {'address': '0xe7B2aC1f9D701Fb98496524a441702D46CE564fc', 'private_key': 'd2fda4b1f76bae5b1ee16c217b81aa424e1dbf50a527b8b321ea4796a35d56a4'}, {'address': '0x9BEc270034797DAeD771AadCC1A5EC0013eC7C98', 'private_key': 'd0a49510dca32a5d6019767b86348c671019bbaf9603dd737a11f9daba42fe49'}, {'address': '0x81c3F4Bb662500240DDdBcbCE98910A9E1414EBb', 'private_key': '60694a14c5e1f216f116317a01d2cf7050cda0b0cf0790e2ac665a825cd5ecb6'}, {'address': '0x799061B7C45a410a5e1E08b251a9e12c7484aB80', 'private_key': '8aa0f518f63f61a579ca57e8aab473d982f43bb624213273257fda7861e6d93e'}, {'address': '0x8DeF2C807D0f7B2a3F757B8DDbe8fa2E6279c565', 'private_key': '5c58329536b732a09919a0e7e2a96c01f6e6866e8904b400215b19dcaff25de9'}, {'address': '0xb64bc024C60233055Bae79c2b398Bd681a9b2309', 'private_key': '2eef58f7260e982cf41e087fe6a42be06726971dbcd6522a4c54f75f7849d8a0'}, {'address': '0xD49341378c7553964453BC9Ab19C864cc6C26713', 'private_key': 'e549f76a25374af8f6104aaca6200aa1badeb86921d9e085dc2de0a15eb7eadb'}, {'address': '0x60265dA65aEe89D296F93D3B88B29921a13b4B31', 'private_key': '4b6b132a8822ecc17d9a831fcb1da4d9dc23c24c8686f129da3f8af717aac855'}, {'address': '0xdAe870e9e003C834b41F5A310C9771c292B046aF', 'private_key': '76a5d1d95b773743c1d8fd44f9ed800003065d01ae66a381d3382393cddfcf5d'}, {'address': '0x966f40444aBE60F2CF0962A9C40c4029c5550c10', 'private_key': 'c0407e780f6b0702655b67be59fe6e29ace56a598d7ea53ff356ffc5344eb1d0'}]


def create_eth_account(infura_url):
    # infura_url = "https://ropsten.infura.io/v3/8d62942fd62641a7ab758673105b6df3"
    web3 = Web3(Web3.HTTPProvider(infura_url))
    account = web3.eth.account.create()
    address = account.address
    private_key = account.privateKey
    return {"address": address, "private_key": private_key}


def create_sawtooth_account(url):
    # url = "http://localhost:8096"
    url += "/user"
    response = requests.post(url, verify=False)
    if response.status_code == 200:
        response_content = json.loads(response.content)
        try:
            if response_content['public_key'] != "" and response_content['private_key'] != "":
                return {"address": response_content['public_key'],
                        "private_key": response_content['private_key']}
        except:
            return {"address": '', "private_key": ''}
    return {"address": '', "private_key": ''}


def save_data(data_path, account_data, num_thread):
    # count_transaction = math.ceil(len(data_path) / num_thread)
    # account_data = elastic.get_account_data(account)
    if account_data['blockchain_type'] == "ETHERUM":
        return save_to_eth_thread(data_path, account_data['account'], account_data["endpoint_url"], num_thread)
    elif account_data['blockchain_type'] == "SAWTOOTH":
        return save_to_sawtooth_thread(data_path, account_data['account'], account_data['encrypted_private_key'],
                                       account_data["endpoint_url"], num_thread)


def save_to_sawtooth_thread(data_path, account, private_key, url, num_thread):
    count_transaction = math.ceil(len(data_path) / num_thread)
    global data_path_global
    global transaction_global
    data_path_global = data_path
    threads = []
    for i in range(0, len(data_path), count_transaction):
        a = threading.Thread(target=save_to_sawtooth_num_thread,
                             args=(data_path[i:i + count_transaction], account, private_key, url))
        a.start()
        threads.append(a)
    for thread in threads:
        thread.join()
    data_path_global_temp = data_path_global.copy()
    data_path_global = []
    transaction_global_temp = transaction_global.copy()
    transaction_global = []

    return {"data_path": data_path_global_temp, "transaction": transaction_global_temp}


def save_to_sawtooth_num_thread(data_path, account, private_key, url):
    for path in data_path:
        save_to_sawtooth(path, account, private_key, url)


def save_to_sawtooth(data_path, account, private_key, url):
    global data_path_global
    global transaction_global
    url += "/synchronize"
    try:
        with open(data_path, 'r') as myfile:
            data = myfile.read()
    except:
        print("path is incorrect")
    data1 = {
        "public_key": account,
        "private_key": private_key,
        "data": data
    }
    response = requests.post(url, json=data1, verify=False)

    if response.status_code == 200:
        response_content = json.loads(response.content)
        try:
            print("success_save_sawtooth " + str(response_content['transaction_id']))
            data_path_global.remove(data_path)
            transaction_global.append(str(response_content['transaction_id']))
            # return {"status": Config().success, "txn_hash": response_content['transaction_id']}
        except:
            # pass
            print("fail_save_sawtooth")
            # return {"status": Config().fail, "txn_hash": ""}
    else:
        # pass
        print("fail_save_sawtooth")
    # return {"status": Config().fail, "txn_hash": ""}


def save_to_eth_thread(data_path, account, infura_url, num_thread):
    count_transaction = math.ceil(len(data_path) / num_thread)
    global data_path_global
    global transaction_global
    threads = []
    data_path_global = data_path
    data_path_per_number = [[] for i in range(0, num_thread)]
    for i in range(0, len(data_path)):
        data_path_per_number[i % num_thread].append(data_path[i])
    for i in range(0, num_thread):
        a = threading.Thread(target=save_to_eth_num_thread,
                             args=(data_path_per_number[i], account, infura_url, i))
        a.start()
        threads.append(a)

    for thread in threads:
        thread.join()
    data_path_global_temp = data_path_global.copy()
    data_path_global = []
    transaction_global_temp = transaction_global.copy()
    transaction_global = []

    return {"data_path": data_path_global_temp, "transaction": transaction_global_temp}


def save_to_eth_num_thread(path, account, infura_url, thread_number):
    for i in range(0, len(path)):
        save_to_eth(path[i], account, infura_url, thread_number)


def save_to_eth(data_path, account, infura_url, thread_number):
    global data_path_global
    global transaction_global
    # infura_url = "https://ropsten.infura.io/v3/8d62942fd62641a7ab758673105b6df3"
    web3 = Web3(Web3.HTTPProvider(infura_url))
    try:
        with open(data_path, 'r') as myfile:
            data = myfile.read()
    except:
        print("path is incorrect")
    nonce = web3.eth.getTransactionCount(account_core[thread_number]['address'])
    signed_txn = web3.eth.account.signTransaction(dict(
        gasPrice=int(web3.eth.gasPrice * 2),
        nonce=nonce,
        gas=510800,
        to=account,
        data=data.encode("utf8").hex(),
    ),
        account_core[thread_number]['private_key']
    )
    try:
        txn_hash = web3.eth.sendRawTransaction(signed_txn.rawTransaction)
    except ValueError as error:
        if error.args[0]['message'][0:17] == 'known transaction':
            data_path_global.remove(data_path)
            transaction_global.append("0x" + error.args[0]['message'].split(" ")[2])
        return 0

    print(str(nonce) + "  " + str(txn_hash.hex()))
    transaction = web3.eth.waitForTransactionReceipt(transaction_hash=txn_hash, timeout=120)
    try:
        if transaction['transactionHash'] == txn_hash:
            print("success_save_eth")
            data_path_global.remove(data_path)
            transaction_global.append(txn_hash.hex())
            # return {"status": Config().success, "txn_hash": txn_hash.hex()}
        else:
            print("fail_save_eth")
            # return {"status": Config().fail, "txn_hash": txn_hash.hex()}
    except:
        print("fail_save_eth")
        # return {"status": Config().fail, "txn_hash": txn_hash.hex()}


def get_data_transaction(transaction_id, blockchain_type, endpoint_url):
    if blockchain_type == "ETHERUM":
        return get_data_eth(transaction_id, endpoint_url)
    elif blockchain_type == "SAWTOOTH":
        return get_data_sawtooth(transaction_id, endpoint_url)


def get_data_eth(transaction_id, infura_url):
    # infura_url = "https://ropsten.infura.io/v3/8d62942fd62641a7ab758673105b6df3"
    web3 = Web3(Web3.HTTPProvider(infura_url))
    transaction = web3.eth.getTransaction(transaction_id)
    data_graph = bytes.fromhex(transaction['input'][2:]).decode('utf8')
    return data_graph


def get_data_sawtooth(transaction_id, url):
    url += "/get_data/" + str(transaction_id)
    response = requests.get(url, verify=False)
    if response.status_code == 200:
        response_content = json.loads(response.content)
        try:
            return response_content['data']
        except:
            return ''
    return ''


def get_data(transaction_id, blockchain_type, endpoint_url, index_name, condition):
    global data_get_return
    transaction_data_string = get_data_transaction(transaction_id, blockchain_type, endpoint_url)
    transaction_data_dict = json.loads(transaction_data_string)
    if index_name in transaction_data_dict:
        for data in transaction_data_dict[index_name]:
            condition_flag = True
            for condition_temp in condition:
                condition_flag = condition_flag and eval(condition_temp[1])
                if not condition_flag:
                    break
            if condition_flag:
                data_get_return.append(data)


def get_data_num_thread(transaction_ids, blockchain_type, endpoint_url, index_name, condition):
    for transaction_id in transaction_ids:
        print(transaction_id)
        get_data(transaction_id, blockchain_type, endpoint_url, index_name, condition)


def get_data_thread(transaction_ids, blockchain_type, endpoint_url, index_name, condition, num_thread):
    global data_get_return
    threads = []
    count_transaction = math.ceil(len(transaction_ids) / num_thread)
    if count_transaction == 0:
        return []
    for i in range(0, len(transaction_ids), count_transaction):
        b = threading.Thread(target=get_data_num_thread,
                             args=(
                                 transaction_ids[i:i + count_transaction], blockchain_type, endpoint_url, index_name,
                                 condition))
        b.start()
        threads.append(b)
    for thread in threads:
        thread.join()
    data_get_return_temp = data_get_return.copy()
    data_get_return.clear()
    return data_get_return_temp
