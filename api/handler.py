from aiohttp.web import json_response
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
import bcrypt
import uuid
import traceback
import logging
import sys
from jsonschema import validate
from json.decoder import JSONDecodeError
import parser as parser
import contract as contract
from Crypto.Cipher import AES
import elastic as elasticsearch
import redis_controller as res
import json
import os
import aiofiles as aiof
from datetime import datetime
import io
import openpyxl
import time

print(os.getcwd())
sys.path.append("../language/processor")
import controller as language_processor
import swagger


def exception_logging(exctype, value, tb):
    """
    Log exception by using the root logger.

    Parameters
    ----------
    exctype : type
    value : NameError
    tb : traceback
    """
    write_val = {'exception_type': str(exctype),
                 'message': str(traceback.format_tb(tb, 10))}
    logging.warn(str(write_val))


sys.excepthook = exception_logging
LOGGER = logging.getLogger(__name__)
logging.basicConfig(filename="logFile.txt",
                    filemode='a',
                    format='%(asctime)s %(levelname)s-%(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)


class Handler(object):

    def __init__(self):
        # self._dir = dir
        self.data_dir = os.getcwd() + "/data"
        super(Handler, self).__init__()

    async def synchronize(self, request):
        file = open("../data_evalution/data_100/sawtooth.txt", "a+")
        if request.headers["Content-Type"].lower() == "application/json":
            body = await self.decode_request(request)
        else:
            data = await request.post()
            body = data.get("mapping").file.read()
            body = json.loads(body.decode())
        account = body['account']
        if len(account) < 50:
            account = account[2:] + "0" * 24
        else:
            account = account + "0" * 62
        address = self.encrypt_private_key(request.app['aes_key'],
                                           request.app['init_vector'].encode().hex(),
                                           bytes.fromhex(account)).hex()
        account_data = elasticsearch.get_account_data(address)
        if len(account_data) == 0:
            return json_response({"status": "error", "detail": "account not found"})
        mapping = body['mapping']
        elasticsearch.create_mapping_blockchain_mapping_record(address, mapping)
        time_start = time.time()
        language_processor.processor(mapping, body['account'])
        time_end = str(time.time() - time_start)
        file.writelines(body['account'] + "++s++" + time_end + "\n")
        file.close()
        return json_response(swagger.parse_swagger())

    async def create_account(self, request):
        body = await self.decode_request(request)
        blockchain_type = body['type'].upper()
        endpoint_url = body["endpoint_url"]
        address = ""
        encrypted_private_key = ""
        if blockchain_type == "ETHERUM":
            account = contract.create_eth_account(endpoint_url)
            address = account['address'][2:] + "0" * 24
            private_key = account['private_key']
            encrypted_private_key = self.encrypt_private_key(request.app['aes_key'],
                                                             request.app['init_vector'].encode().hex(),
                                                             private_key).hex()
        elif blockchain_type == "SAWTOOTH":
            account = contract.create_sawtooth_account(endpoint_url)
            address = account['address'] + "0" * 62
            private_key = account['private_key']
            encrypted_private_key = self.encrypt_private_key(request.app['aes_key'],
                                                             request.app['init_vector'].encode().hex(),
                                                             bytes.fromhex(private_key)).hex()
        address = self.encrypt_private_key(request.app['aes_key'],
                                           request.app['init_vector'].encode().hex(),
                                           bytes.fromhex(address)).hex()

        return_elas = elasticsearch.create_blockchain_record(address, encrypted_private_key, blockchain_type,
                                                             endpoint_url)
        return json_response({
            "status": return_elas['result'],
            "account": account['address']
        })

    async def get_data(self, request):
        time_start = time.time()
        num_thread = 16
        if request.headers["Content-Type"].lower() == "application/json":
            body = await self.decode_request(request)
        else:
            data = await request.post()
            body = data.get("mapping").file.read()
            body = json.loads(body.decode())
        account = body.get("account")
        if len(account) < 50:
            account = account[2:] + "0" * 24
        else:
            account = account + "0" * 62
        address = self.encrypt_private_key(request.app['aes_key'],
                                           request.app['init_vector'].encode().hex(),
                                           bytes.fromhex(account)).hex()
        account_data = elasticsearch.get_account_data(address)
        if len(account_data) == 0:
            return json_response({"status": "error", "detail": "account not found"})

        index_name = body.get("identifier")
        filter_condition = body.get("filter")
        blockchain_type = account_data['blockchain_type']
        endpoint_url = account_data['endpoint_url']
        condition = []
        for i in range(0, len(filter_condition)):
            condition.append(filter_condition[i].replace(" ", "").split("->"))
            attribute_array = condition[i][0].split(".")
            str_replace = "data"
            for attribute in attribute_array:
                str_replace += "['" + attribute + "']"
            condition[i][1] = condition[i][1].replace(" ", "").replace(condition[i][0], str_replace)
        transaction_ids = elasticsearch.fetch_transaction_id(address)
        data_return = contract.get_data_thread(transaction_ids, blockchain_type, endpoint_url, index_name, condition,
                                               num_thread)
        time_end = str(time.time() - time_start)
        file = open("../data_evalution/data_100/sawtooth.txt", "a+")
        file.writelines(body.get("account") + "++g++" + time_end + "\n")
        file.close()
        return json_response({
            "data": data_return
        })

    async def decode_request(self, request):
        try:
            return await request.json()
        except JSONDecodeError:
            raise print('error')

    def encrypt_private_key(self, aes_key, public_key, private_key):
        init_vector = bytes.fromhex(public_key[:32])
        cipher = AES.new(bytes.fromhex(aes_key), AES.MODE_CBC, init_vector)
        return cipher.encrypt(private_key)

    def decrypt_private_key(self, aes_key, public_key, encrypted_private_key):
        init_vector = bytes.fromhex(public_key[:32])
        cipher = AES.new(bytes.fromhex(aes_key), AES.MODE_CBC, init_vector)
        private_key = cipher.decrypt(bytes.fromhex(encrypted_private_key))
        return private_key


def get_time():
    dateTimeObj = datetime.now()
    return dateTimeObj.strftime("%d_%b_%Y_%H_%M_%S_%f")
